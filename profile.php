<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/main_style.css">
		<link rel="stylesheet" href="css/profile_style.css">
		<link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
		<title>Профиль</title>
	</head>
	<body>
		<div class="backgroundImage">
			<div class="backgroundStyle" id="backgroundImageOne"></div>
			<div class="backgroundStyle" id="backgroundImageTwo"></div>
			<div class="backgroundStyle" id="backgroundImageThree"></div>
			<div class="backgroundStyle" id="backgroundImageFour"></div>
		</div>
		<header class="hat">
			<!--<div class="banner"></div>-->
			<div class="menu">
				<button onclick="window.location.href='https://breakover.ru/d.dzyad/project/myProject.php'">Мои проекты</button>
				<!--<button onclick="window.location.href='https://breakover.ru/d.dzyad/roadMap.php'">Планы</button>-->
				<button onclick="window.location.href='https://breakover.ru/d.dzyad/'">
					<i class="fa fa-home" aria-hidden="true"></i>
				</button>
				<!--<button onclick="window.location.href='https://breakover.ru/d.dzyad/tasks.php'">Задания</button>-->
				<button onclick="window.location.href='https://breakover.ru/d.dzyad/profile.php'">Профиль</button>
			</div>
		</header>
		<div class="backPanel">
			<a href="https://breakover.ru/d.dzyad">
				<i class="fa fa-reply-all" aria-hidden="true"></i>
				<p>Назад</p>
			</a>
		</div>
		<div class="scrollPanel hide">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
			<p>Наверх</p>
		</div>
		<div class="profiles">
			<div class="block borders">
				<div class="imgage borders"></div>
				<div class="nickname borders">
					<p>Dikhicher</p>
				</div>
				<div class="discriptions borders">
					<p>Студент 3-го курса "Щелковского колледжа", по специальности веб-разработчик и разработчик мультимедийных приложений</p>
				</div>
				<div class="skills borders">
					<div><i class="fa fa-code" aria-hidden="true">Skills</i><progress value="281" max="600"></progress></div>
					<div><i class="fa fa-html5" aria-hidden="true"> HTML 5</i><progress value="73" max="100"></progress></div>
					<div><i class="fa fa-css3" aria-hidden="true"> CSS</i><progress value="38" max="100"></progress></div>
					<div><i> JS</i><progress value="18" max="100"></progress></div>
					<div><i> BluePrints</i><progress value="52" max="100"></progress></div>
					<div><i> C++</i><progress value="42" max="100"></progress></div>
					<div><i class="fa fa-git" aria-hidden="true"></i><progress value="58" max="100"></progress></div>
				</div>
			</div>
		</div>
		<div class="spanFoot"></div>
		<div class="foot">
			<button><i onclick="window.open('https://www.instagram.com/soulessskull')" class="fa fa-instagram" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://vk.com/soullessskull')" class="fa fa-vk" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://t.me/dikhicher')" class="fa fa-telegram" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://www.twitch.tv/Dikhicher')" class="fa fa-twitch" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://www.youtube.com/channel/UCk4GbmGIVfNAVFQ2_7xBudg')" class="fa fa-youtube" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://vk.com/away.php?to=https%3A%2F%2Fopen.spotify.com%2Fuser%2F31bf47zh624cjhux5sednior3lxq%3Fsi%3DeO1Fn2azStemWroGfNig-Q%26utm_source%3Dnative-share-menu%26dl_branch%3D1&cc_key=')" class="fa fa-spotify" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://steamcommunity.com/profiles/76561198110676450')" class="fa fa-steam" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://gitlab.com/Dixicher')" class="fa fa-gitlab" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://trello.com/b/U4kD8yAq/%D0%B4%D0%B8%D0%B7%D0%B4%D0%BE%D0%BA')" class="fa fa-trello" aria-hidden="true"></i></button>
			<button><i onclick="window.open('https://server253.hosting.reg.ru/phpmyadmin/index.php')" class="fa fa-database" aria-hidden="true"></i></button>
		</div>
		<script type="text/javascript" src="js/scroll_to_up.js"></script>
		<script type="text/javascript" src="js/profile.js"></script>
	</body>
</html>