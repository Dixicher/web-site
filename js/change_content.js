changeContent('./main/main.php', '.content');

function getIt(address, conteiner) {
	let content = new XMLHttpRequest();
	content.open('GET', address, false);
	content.send();

	let contentRequest = content.response.match(/(?<=<div class="new_content">)([\s\S]*?)(?=<\/div>)/gm);

	document.querySelector(conteiner).innerHTML = contentRequest;
}

function changeContent(address, conteiner){
	let content = new XMLHttpRequest();
	content.open('POST', address, false);
	content.send();
	document.querySelector(conteiner).innerHTML = content.response;
	if (address == './main/main.php'){
		getIt('./main/news.php', '.news')
		getIt('./main/update.php', '.update')
		getIt('./main/plans.php', '.plans')
	}
}