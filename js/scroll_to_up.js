let element = document.querySelector(".scrollPanel");

document.getElementsByTagName("body")[0].onscroll = function(){
	if ((window.pageYOffset  / 100) >= 1) {
		element.classList.remove("hide");
		element.classList.add("visible");
	}
	else {
		element.classList.remove("visible");
		element.classList.add("hide");
	}
}

element.onclick = function(){
	window.scrollTo(0, 0);
}