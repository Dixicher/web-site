function start(property) {
	let elementText = document.getElementById(property);
	elementText.classList.remove("stop");
	elementText.classList.add("start");
}

function stop(property) {
	let element = document.getElementById(property);
	element.classList.remove("start");
	element.classList.add("stop");
}